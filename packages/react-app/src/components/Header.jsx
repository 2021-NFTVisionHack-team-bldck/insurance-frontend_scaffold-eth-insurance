import { PageHeader } from "antd";
import React from "react";

// displays a page header

export default function Header() {
  return (
    <a href="https://gitlab.com/nftvisionhack/insurance-frontend_scaffold-eth-insurance" target="_blank" rel="noopener noreferrer">
      <PageHeader
        title="TEAM BLDCK"
        subTitle="Pet Insurance"
        style={{ cursor: "pointer" }}
      />
    </a>
  );
}
